# Mini redis

A really simple redis-like database
# How to use
First install the package
```bash
yarn global add tiny-redis
```
And then run it
```bash
// Server
tiny-redis-server # <address: optional>
// Client
tiny-redis-client # <address>
```
# Commands:
 - `set <key> <value>`
 - `get <key>`
 - `delete <key>`

# **DISCLAIMER**
**I don't recomend using this in production, this is just a toy project so it may have serious security holes**
