#!/bin/node
import assert from "assert";
import { createConnection } from "net";
import * as readline from "readline"
if(!process.argv[2]){
  console.error("Use: %s <host>:<port>", process.argv[1]);
  process.exit(0);
}
const client = createConnection({
    host: process.argv[2].split(":")[0],
    port: Number.parseInt(process.argv[2].split(":")[1])
});
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: "mini-redis > "
});
rl.prompt();
rl.on("line", line => {
    if(!line){
        rl.prompt();
        return;
    }
    client.write(line);
})
client.on("data", (buf) => {
    const str = buf.toString("utf-8");
    console.log(str);
    rl.prompt();
})
client.on("end", ()=> {
    console.log("Goodbye!");
    process.exit(0);
})
client.on("error", e => {
    console.log("Error while connecting to the server!");
    process.exit(0);
})
