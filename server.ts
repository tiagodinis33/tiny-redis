#!/bin/node
import assert from "assert"
import {createServer, Socket, SocketAddress} from "net"
type Command = (args: string[]) => string;
type Commands = {[name: string]: Command}

var db: {[key: string]: string} = {

}

const commands: Commands = {
    set(args: string[]){
        db[args[0]] = args[1];
        return "Command ran successfully!"
    },
    get(args: string[]){
        if(!(args[0] in db)){
            return `The key "${args[0]}" was not found`
        }
        return String(db[args[0]]);
    },
    delete(args: string[]){
        if(!(args[0] in db)){
            return `The key "${args[0]}" was not found`
        }
        delete db[args[0]];
        return "Command ran successfully";
    }
}
function onConnection(socket: Socket): void{
    socket.on("data", (buf)=>{
        const str = buf.toString("utf-8");
        if(str == "exit"){
            socket.end();
            return;
        }
        var split = str.split(" ");
        const command = commands[split[0]];
        if(!command){
            socket.write("Command not found!");
            return;
        }
        socket.write(command(split.slice(1)));
    })
}

const exceptions: {[code: string]: () => void} = {
    ERR_SOCKET_BAD_PORT(){
        console.error("Invalid port! use a port which 0 <= \ud835\udc65 < 65536")
    },
    ENOTFOUND(){
        console.log("Invalid address!")
    }
}
if(!process.argv[2]){
  process.argv[2] = ''
}
const host = process.argv[2].split(":")[0];
const server = createServer(onConnection);
const port = Number.parseInt(process.argv[2].split(":")[1]);

try {
    server.listen(Number.isNaN(port)? undefined: port, host, ()=> {
    console.log("Listening on address: %s:%d", (server.address() as any).address, (server.address() as any).port)
    })
} catch(e: any) {
    const f = exceptions[e.code];
    if(f){
        f();
    } else {
        throw e;
    }
}
